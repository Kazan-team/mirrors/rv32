"""
/*
 * Copyright 2018 Jacob Lifshay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
"""

from migen import Constant

cause_instruction_address_misaligned = Constant(0x0, 4)
cause_instruction_access_fault = Constant(0x1, 4)
cause_illegal_instruction = Constant(0x2, 4)
cause_breakpoint = Constant(0x3, 4)
cause_load_address_misaligned = Constant(0x4, 4)
cause_load_access_fault = Constant(0x5, 4)
cause_store_amo_address_misaligned = Constant(0x6, 4)
cause_store_amo_access_fault = Constant(0x7, 4)
cause_user_environment_call = Constant(0x8, 4)
cause_supervisor_environment_call = Constant(0x9, 4)
cause_machine_environment_call = Constant(0xB, 4)
cause_instruction_page_fault = Constant(0xC, 4)
cause_load_page_fault = Constant(0xD, 4)
cause_store_amo_page_fault = Constant(0xF, 4)

class OP:
    """  Opcode constants
    """
    load = Constant(0x03, 7)
    load_fp = Constant(0x07, 7)
    custom_0 = Constant(0x0B, 7)
    misc_mem = Constant(0x0F, 7)
    op_imm = Constant(0x13, 7)
    auipc = Constant(0x17, 7)
    op_imm_32 = Constant(0x1B, 7)
    op_48b_escape_0 = Constant(0x1F, 7)

    store = Constant(0x23, 7)
    store_fp = Constant(0x27, 7)
    custom_1 = Constant(0x2B, 7)
    amo = Constant(0x2F, 7)
    op = Constant(0x33, 7)
    lui = Constant(0x37, 7)
    op_32 = Constant(0x3B, 7)
    op_64b_escape = Constant(0x3F, 7)

    madd = Constant(0x43, 7)
    msub = Constant(0x47, 7)
    nmsub = Constant(0x4B, 7)
    nmadd = Constant(0x4F, 7)
    op_fp = Constant(0x53, 7)
    reserved_10101 = Constant(0x57, 7)
    rv128_0 = Constant(0x5B, 7)
    op_48b_escape_1 = Constant(0x5F, 7)

    branch = Constant(0x63, 7)
    jalr = Constant(0x67, 7)
    reserved_11010 = Constant(0x6B, 7)
    jal = Constant(0x6F, 7)
    system = Constant(0x73, 7)
    reserved_11101 = Constant(0x77, 7)
    rv128_1 = Constant(0x7B, 7)
    op_80b_escape = Constant(0x7F, 7)

class F3:
    """ Funct3 constants
    """
    jalr = Constant(0x0, 3)
    beq = Constant(0x0, 3)
    bne = Constant(0x1, 3)
    blt = Constant(0x4, 3)
    bge = Constant(0x5, 3)
    bltu = Constant(0x6, 3)
    bgeu = Constant(0x7, 3)

    lb = Constant(0x0, 3)
    lh = Constant(0x1, 3)
    lw = Constant(0x2, 3)
    lbu = Constant(0x4, 3)
    lhu = Constant(0x5, 3)

    sb = Constant(0x0, 3)
    sh = Constant(0x1, 3)
    sw = Constant(0x2, 3)

    addi = Constant(0x0, 3)
    slli = Constant(0x1, 3)
    slti = Constant(0x2, 3)
    sltiu = Constant(0x3, 3)
    xori = Constant(0x4, 3)
    srli_srai = Constant(0x5, 3)
    ori = Constant(0x6, 3)
    andi = Constant(0x7, 3)

    add_sub = Constant(0x0, 3)
    sll = Constant(0x1, 3)
    slt = Constant(0x2, 3)
    sltu = Constant(0x3, 3)
    xor = Constant(0x4, 3)
    srl_sra = Constant(0x5, 3)
    _or = Constant(0x6, 3)
    _and = Constant(0x7, 3)

    fence = Constant(0x0, 3)
    fence_i = Constant(0x1, 3)

    ecall_ebreak = Constant(0x0, 3)
    csrrw = Constant(0x1, 3)
    csrrs = Constant(0x2, 3)
    csrrc = Constant(0x3, 3)
    csrrwi = Constant(0x5, 3)
    csrrsi = Constant(0x6, 3)
    csrrci = Constant(0x7, 3)

csr_ustatus = Constant(0x000, 12)
csr_fflags = Constant(0x001, 12)
csr_frm = Constant(0x002, 12)
csr_fcsr = Constant(0x003, 12)
csr_uie = Constant(0x004, 12)
csr_utvec = Constant(0x005, 12)
csr_uscratch = Constant(0x040, 12)
csr_uepc = Constant(0x041, 12)
csr_ucause = Constant(0x042, 12)
csr_utval = Constant(0x043, 12)
csr_uip = Constant(0x044, 12)
csr_cycle = Constant(0xC00, 12)
csr_time = Constant(0xC01, 12)
csr_instret = Constant(0xC02, 12)
csr_cycleh = Constant(0xC80, 12)
csr_timeh = Constant(0xC81, 12)
csr_instreth = Constant(0xC82, 12)

csr_sstatus = Constant(0x100, 12)
csr_sedeleg = Constant(0x102, 12)
csr_sideleg = Constant(0x103, 12)
csr_sie = Constant(0x104, 12)
csr_stvec = Constant(0x105, 12)
csr_scounteren = Constant(0x106, 12)
csr_sscratch = Constant(0x140, 12)
csr_sepc = Constant(0x141, 12)
csr_scause = Constant(0x142, 12)
csr_stval = Constant(0x143, 12)
csr_sip = Constant(0x144, 12)
csr_satp = Constant(0x180, 12)

csr_mvendorid = Constant(0xF11, 12)
csr_marchid = Constant(0xF12, 12)
csr_mimpid = Constant(0xF13, 12)
csr_mhartid = Constant(0xF14, 12)
csr_mstatus = Constant(0x300, 12)
csr_misa = Constant(0x301, 12)
csr_medeleg = Constant(0x302, 12)
csr_mideleg = Constant(0x303, 12)
csr_mie = Constant(0x304, 12)
csr_mtvec = Constant(0x305, 12)
csr_mcounteren = Constant(0x306, 12)
csr_mscratch = Constant(0x340, 12)
csr_mepc = Constant(0x341, 12)
csr_mcause = Constant(0x342, 12)
csr_mtval = Constant(0x343, 12)
csr_mip = Constant(0x344, 12)
csr_mcycle = Constant(0xB00, 12)
csr_minstret = Constant(0xB02, 12)
csr_mcycleh = Constant(0xB80, 12)
csr_minstreth = Constant(0xB82, 12)

csr_dcsr = Constant(0x7B0, 12)
csr_dpc = Constant(0x7B1, 12)
csr_dscratch = Constant(0x7B2, 12)

